package ru.kuzin.tm.api.service;

import ru.kuzin.tm.api.repository.IProjectRepository;
import ru.kuzin.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project updateById (String id, String name, String description);

    Project updateByIndex (Integer index, String name, String description);

}
