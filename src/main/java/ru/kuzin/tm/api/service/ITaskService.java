package ru.kuzin.tm.api.service;

import ru.kuzin.tm.api.repository.ITaskRepository;
import ru.kuzin.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task updateById (String id, String name, String description);

    Task updateByIndex (Integer index, String name, String description);

}
