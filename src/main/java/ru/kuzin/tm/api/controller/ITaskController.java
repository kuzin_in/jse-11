package ru.kuzin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTasks();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskByIndex();

    void updateTaskById();

}
