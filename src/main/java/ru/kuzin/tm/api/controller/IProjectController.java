package ru.kuzin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProjects();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

}
